// import Demo2 from './pages/demo2/Demo2.tsx';
import React, { useState } from 'react';
import Demo1 from './pages/demo1/Demo1.tsx';
import Demo2 from './pages/demo2/Demo2.tsx';
import { Demo3 } from './pages/demo3/Demo3.tsx';
import { Demo4List } from './pages/demo4-list/Demo4List.tsx';
import { Demo5FetchList } from './pages/demo5/Demo5FetchList.tsx';
import { Demo6 } from './pages/demo6/Demo6.tsx';


const COMPONENTS: { [key: string]: { component: React.ReactNode, label: string}  } = {
  demo1: {
    component: <Demo1/>,
    label: 'demo1'
  },
  demo2: {
    component: <Demo2/>,
    label: 'demo2'
  },
  demo3: {
    component: <Demo3/>,
    label: 'demo3'
  },
  demo4: {
    component: <Demo4List/>,
    label: 'demo4',
  },
  demo5: {
    component: <Demo5FetchList/>,
    label: 'demo5',
  },
  demo6: {
    component: <Demo6/>,
    label: 'demo6',
  },
}

const App = () => {
  const [page, setPage] = useState('demo5');

  return (
    <div>
      <h1 className="title">Demo</h1>
      {
        Object.values(COMPONENTS).map(item => {
          return <button key={item.label} onClick={() => setPage(item.label)}>{item.label}</button>
        })
      }
      {COMPONENTS[page].component}
    </div>
  )
}

export default App;
