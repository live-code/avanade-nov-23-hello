import React from 'react';

export default function Demo2() {
  function getText(event: React.KeyboardEvent<HTMLInputElement>) {
    if (event.key === 'Enter') {
      window.open(event.currentTarget.value)
    }
  }

  return <>
    <h1>Demo 2</h1>
    <input type="text" onKeyUp={getText} />
  </>
}

/*
document.getElementById('btn')
.addEventListener('click', doSomething)

function doSomething(e: MouseEvent) {
  console.log(' do something ')
}
*/
