import { useState } from 'react';
import EmptyMsg from './components/EmptyMsg.tsx';
import { Products } from './components/Products.tsx';

const Demo1 = () => {
  const [totalProducts, setTotalProducts] = useState(0);

  function inc() {
    // setTotalProducts(totalProducts + 1)
    setTotalProducts(s => s + 10)
  }

  return (
    <>
      <div className="panel">
        {
          totalProducts ?
            <Products total={totalProducts} /> :
            <EmptyMsg />
        }

        <button onClick={inc}>+</button>
      </div>
    </>
  )
}

export default Demo1;
