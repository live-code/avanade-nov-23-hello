
interface ProductsProps {
  total: number;
}

export function Products(props: ProductsProps) {

  return <div>
    <h1>There are {props.total} products</h1>
  </div>
}
