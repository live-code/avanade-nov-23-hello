import { useState } from 'react';

export function Demo3() {
  const [counter, setCounter ] = useState(0);

  function inc() {
    setCounter(counter + 1)
  }

  return <div>
    <h1>Demo 3</h1>
    <button onClick={inc}>{counter}</button>
  </div>
}


/*
function useStateNew() {
  return [10, 20, 30]
}

const state = useStateNew()
const [x, y, z ] = state;
//const x = state[0];
// const y = state[1]

console.log(x, y, z )
*/
