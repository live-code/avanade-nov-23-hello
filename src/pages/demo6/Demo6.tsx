import { useState } from 'react';
import { Card } from '../../shared/card/Card.tsx';


export function Demo6() {
  const [visible, setVisible] = useState(false)

  const [counter, setCounter] = useState(0)

  console.log('parent render')
  return (
    <div className="mx-5">
      <h1>Reusable Card Component </h1>

      <button onClick={() => setCounter(c => c+1)}>+</button>

      <Card
        title={'My  Card ' + counter}
        icon="fa fa-hand-peace-o"
        theme="dark"
        img="https://www.adorama.com/alc/wp-content/uploads/2018/11/landscape-photography-tips-yosemite-valley-feature.jpg"
        buttonLabel="show Details"
        buttonUrl="https://www.fabiobiondi.dev"
        onButtonClick={() => setVisible(c => !c)}
        onIconClick={() => {
          window.alert('hello! ')
        }}
      >
        bla bla bla
      </Card>

        <br/>
        {
          visible &&
            <Card
              title="My AMazing Card"
              theme="light"
              buttonLabel="Website"
              onButtonClick={() => {
                window.open('https://www.fabiobiondi.dev')
              }}
            >
              efewfe
            </Card>
        }
  </div>
)}
