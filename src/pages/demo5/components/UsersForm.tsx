import clsx from 'clsx';
import { useEffect, useRef, useState } from 'react';
import { User } from '../../../model/user.ts';

interface UsersFormProps {
  onAddUser: (data: Partial<User>) => void;
}
export function UsersForm(props: UsersFormProps) {

  const [formValidator, setFormValidator] = useState({
    nameValid: false,
    emailValid: false,
    formValid: false
  });

  const nameInput = useRef<HTMLInputElement>(null);
  const emailInput = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if(nameInput.current) {
      nameInput.current.focus()
    }
  }, []);

  function addUser() {

    if (
      nameInput.current && emailInput.current
    ) {
      props.onAddUser({
        name: nameInput.current.value,
        email: emailInput.current.value
      })

      nameInput.current.value = ''
      emailInput.current.value = ''
    }

  }

  function onChangeHandler() {
    const isNameValid = nameInput.current!.value.length > 3
    const isEmailValid = emailInput.current!.value.length > 3

    setFormValidator({
      nameValid: isNameValid,
      emailValid: isEmailValid,
      formValid: isNameValid && isEmailValid
    })
  }

  console.log('render child')
  return <div>
    <div>
      <input
        type="text" placeholder="name" ref={nameInput} onInput={onChangeHandler}
        className={clsx('form-control', {
          'is-valid': formValidator.nameValid,
          'is-invalid': !formValidator.nameValid,
        })}
      />
      <input
        type="text" placeholder="user email" ref={emailInput}  onInput={onChangeHandler}
        className={clsx('form-control', {
          'is-valid': formValidator.emailValid,
          'is-invalid': !formValidator.emailValid,
        })}
      />
    </div>

    <button
      className="btn btn-primary"
      disabled={!formValidator.formValid}
      onClick={addUser}>Add User</button>

  </div>
}


