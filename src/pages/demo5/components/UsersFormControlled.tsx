import clsx from 'clsx';
import React, { useState } from 'react';
import { User } from '../../../model/user.ts';

interface UsersFormProps {
  onAddUser: (data: Partial<User>) => void;
}
export function UsersFormControlled(props: UsersFormProps) {
  const [formData, setFormData] = useState({ name: '', email: '', phone: '', gender: ''})

  function addUser() {
   props.onAddUser(formData)
  }

  function onChangeHandler(event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    const value = event.currentTarget.value;
    const nameAttr = event.currentTarget.name;
    setFormData(prevState => ({ ...prevState, [nameAttr]: value }))
  }


  const isNameValid = formData.name.length > 3;
  const isEmailValid = formData.email.length > 3;
  const isFormValid = isNameValid && isEmailValid;

  return (
    <div>
      <div>
        <input
          name="name"
          type="text" placeholder="name"
          className={clsx('form-control', {
            'is-valid': isNameValid,
            'is-invalid': !isNameValid,
          })}
          value={formData.name}
          onChange={onChangeHandler}
        />
        <input
          name="email"
          type="text" placeholder="email"
          className={clsx('form-control', {
            'is-valid': isEmailValid,
            'is-invalid': !isEmailValid,
          })}
          value={formData.email}
          onChange={onChangeHandler}
        />
        <input
          name="phone"
          type="text" placeholder="email"
          value={formData.phone}
          onChange={onChangeHandler}
        />

        <select
          name="gender"
          value={formData.gender}
          onChange={onChangeHandler}
        >
          <option value="">Select Gender</option>
          <option value="M">Male</option>
          <option value="F">Female</option>
        </select>
      </div>

      <button
        className="btn btn-primary"
        disabled={!isFormValid}
        onClick={addUser}>Add User</button>

      <pre>{JSON.stringify(formData)}</pre>
    </div>
  )
}
