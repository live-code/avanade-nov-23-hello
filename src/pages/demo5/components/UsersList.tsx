import { User } from '../../../model/user.ts';

interface UsersListProps {
  data: User[];
  onDeleteUser: (id: number) => void;
}
export function UsersList(props: UsersListProps) {
  return (

    <ul className="list-group">
      {
        props.data.map(user => (
          <li key={user.id} className="list-group-item d-flex justify-content-between">
            {user.name} - {user.email} - {user.phone}
            <button
              className="btn btn-primary"
              onClick={() => props.onDeleteUser(user.id)}>Delete</button>
          </li>
        ))
      }
    </ul>
  )
}
