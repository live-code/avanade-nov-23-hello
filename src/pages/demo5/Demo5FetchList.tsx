import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../model/user.ts';
import { Spinner } from '../../shared/Spinner.tsx';
import { UsersForm } from './components/UsersForm.tsx';
import { UsersFormControlled } from './components/UsersFormControlled.tsx';
import { UsersList } from './components/UsersList.tsx';
const api = 'https://jsonplaceholder.typicode.com';

// LOCAL CSS, LOCAL SCOPED CSS, CSS MODULES
import css from './Demo5FetchList.module.css';

export function Demo5FetchList() {
  const [users, setUsers] = useState<User[]>([])
  const [error, setError] = useState('')
  const [pending, setPending] = useState(false)

  useEffect(() => {
    loadUsers()
  }, [])

  function loadUsers() {
    setError('')
    setPending(true)
    axios.get<User[]>(`${api}/users`)
      .then(result => {
        setUsers(result.data)
      })
      .catch(e => {
        setError(e.message)
        setUsers([])
      })
      .finally(() => setPending(false))
  }

  function deleteUser(id: number) {
    axios.delete(`${api}/users/${id}`)
      .then(() => setUsers(users.filter(item => item.id !== id)))
      .catch((e) => setError(e))
  }

  function addUser(data: Partial<User>) {
    setError('')
    setPending(true)
    axios.post<User>(`${api}/users`, data)
      .then((result) => setUsers([...users, result.data]))
      .catch((e) => setError(e))
      .finally(() => setPending(false))
  }

  return <div>
    <h1 className={css.title}>Demo 5 Fetch LIst</h1>
    { pending && <Spinner />}
    { error && <div>{error}</div> }
    <UsersFormControlled onAddUser={addUser}  />
    {/*<UsersForm onAddUser={addUser} />*/}
    <UsersList data={users} onDeleteUser={deleteUser}/>
  </div>
}
