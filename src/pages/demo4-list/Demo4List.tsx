import { useState } from 'react';

/**
 * Simple List (No fetch)
 */

const initialState = [
  { id: 1, name: 'Fabio' },
  { id: 2, name: 'Lorenzo' },
  { id: 3, name: 'Silvia' }
];

export function Demo4List() {
  const [users, setUsers] = useState(initialState)

  function addUser() {
    const newArray = [...users, { id: Date.now(), name: 'ciccio'}]
    setUsers(newArray)
  }

  function deleteUser(id: number) {
    const newArray = users.filter(item => item.id !== id)
    setUsers(newArray)
  }

  return <div>
    <h1>Simple List</h1>

    <ul>
    {
      users.map((item) => (
        <li key={item.id}>
          {item.name} - {item.id}
          <button onClick={() => deleteUser(item.id)}>delete</button>
        </li>
      ))
    }
    </ul>


    <button onClick={addUser}>add</button>


  </div>
}
