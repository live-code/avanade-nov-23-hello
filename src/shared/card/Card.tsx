// import css from './Card.module.css';
import clsx from 'clsx';
import { PropsWithChildren, useState } from 'react';

type CardProps = {
  title: string;
  icon?: string;
  img?: string;
  buttonLabel?: string;
  buttonUrl?: string;
  theme?: 'dark' | 'light';
  onIconClick?: () => void;
  onButtonClick?: () => void;
}

export function Card(props: PropsWithChildren<CardProps>) {
  const [show, setShow] = useState(true);
  return (
    <div
      className={clsx('card', {
        'bg-dark': props.theme === 'dark',
        'bg-light': props.theme === 'light',
      })}
    >
      {
        props.img &&
          <img
            className="card-img-top"
            src={props.img} alt={props.title}
          />
      }
      <div
        className={clsx('card-body', {
          'text-white': props.theme === 'dark',
          'text-black': props.theme === 'light',
        })}
      >
        <h5 className="card-title">
          <div className="d-flex justify-content-between">
            <div onClick={() => setShow(s => !s)}>{props.title}</div>
            {
              props.icon &&
                <i className={props.icon} onClick={props.onIconClick}></i>
            }
          </div>
        </h5>

        {
          show &&
            <>
              <p className="card-text">
                {props.children}
              </p>

              <div className="d-flex justify-content-end">
                  {
                    props.buttonLabel &&
                    <button
                    className="btn btn-primary"
                    onClick={props.onButtonClick}
                >
                  {props.buttonLabel}
                </button>
                }
              </div>
            </>
        }

      </div>
    </div>
  )
}
